# AI Technical Papers
Links to Frameworks, Libraries,  Datasets, White papers, articles, list of books, algorithms, tutorials, diagrams, models, code examples, videos, glossaries, and other topics related to Projects in Deep Learning, Machine Learning and AI. 

## Machine Learning Guides
![Relation between AI, ML and DL](https://github.com/aridiosilva/My-TensorFlow-tutorials/blob/master/Relation%20Between%20AI%2C%20Machine%20Learning%20and%20Deep%20Learning.png)

               source (https://www.prowesscorp.com/wp-content/uploads/2017/06/inBlog.png)

Simple step-by-step walkthroughs to solve common machine learning problems using best practices

- [*Overview*](https://developers.google.com/machine-learning/guides/)
- [*Rules of Machine Learning*](https://developers.google.com/machine-learning/guides/rules-of-ml/)
- [*Text classification*](https://developers.google.com/machine-learning/guides/text-classification/)
- [*Homemade Machine Learning*](https://github.com/trekhleb/homemade-machine-learning)

## Simplified Machine Learning Map
With new neural network architectures popping up every now and then, it’s hard to keep track of them all. Knowing all the abbreviations being thrown around (DCIGN, BiLSTM, DCGAN, anyone?) can be a bit overwhelming at first. Composing a complete list is practically impossible, as new architectures are invented all the time. 

![Machine Learning Map*](https://gitlab.com/aridiosilva/ai-technical-papers/blob/master/machine-learning-map.png)

                source (https://github.com/trekhleb/homemade-machine-learning)

## Papers and Simplified Chart about Different Neural Networks

![*chart of different neural networks*](https://gitlab.com/aridiosilva/ai-technical-papers/blob/master/chart_of_different_neural_networks.png)

                                source (http://www.asimovinstitute.org/neural-network-zoo/)
                
 - [*Kohonen networks (KN), also self organising (feature) map, SOM, SOFM)*](http://cioslab.vcu.edu/alg/Visualize/kohonen-82.pdf) - Kohonen, Teuvo. “Self-organized formation of topologically correct feature maps.” Biological cybernetics 43.1 (1982)
 - [*Support vector machines (SVM)*](http://image.diku.dk/imagecanon/material/cortes_vapnik95.pdf) - Cortes, Corinna, and Vladimir Vapnik. “Support-vector networks.” Machine learning 20.3 (1995): 273-297.
 - [*Liquid state machines (LSM)*](https://web.archive.org/web/20120222154641/http://ramsesii.upf.es/seminar/Maass_et_al_2002.pdf) - Maass, Wolfgang, Thomas Natschläger, and Henry Markram. “Real-time computing without stable states: A new framework for neural computation based on perturbations (Wayback Machine) - Neural computation 14.11 (2002): 2531-2560.
 - [*Extreme learning machines (ELM)*](http://www.ntu.edu.sg/home/egbhuang/pdf/ELM-Rosenblatt-Neumann.pdf) - Cambria, Erik, et al. “Extreme learning machines [trends & controversies].” IEEE Intelligent Systems 28.6 (2013): 30-59.
 - [*Echo state networks (ESN)*](https://pdfs.semanticscholar.org/8922/17bb82c11e6e2263178ed20ac23db6279c7a.pdf) - Jaeger, Herbert, and Harald Haas. “Harnessing nonlinearity: Predicting chaotic systems and saving energy in wireless communication.” science 304.5667 (2004): 78-80.
 - [*Deep residual networks (DRN) *](https://arxiv.org/pdf/1512.03385v1.pdf) - He, Kaiming, et al. “Deep residual learning for image recognition.” arXiv preprint arXiv:1512.03385 (2015).
 - [*Bidirectional recurrent neural networks, bidirectional long & short term memory networks and bidirectional gated recurrent units (BiRNN, BiLSTM and BiGRU respectively)*](http://www.di.ufpe.br/~fnj/RNA/bibliografia/BRNN.pdf) - Schuster, Mike, and Kuldip K. Paliwal. “Bidirectional recurrent neural networks.” IEEE Transactions on Signal Processing 45.11 (1997): 2673-2681.
 - [*Neural Turing machines (NTM)*](https://arxiv.org/pdf/1410.5401v2.pdf) - Graves, Alex, Greg Wayne, and Ivo Danihelka. “Neural turing machines.” arXiv preprint arXiv:1410.5401 (2014).
 - [*Gated recurrent units (GRU)*](https://arxiv.org/pdf/1412.3555v1.pdf) - Chung, Junyoung, et al. “Empirical evaluation of gated recurrent neural networks on sequence modeling.” arXiv preprint arXiv:1412.3555 (2014).
 - [*Long/short term memory (LSTM) networks*](https://www.bioinf.jku.at/publications/older/2604.pdf) - Hochreiter, Sepp, and Jürgen Schmidhuber. “Long short-term memory.” Neural computation 9.8 (1997): 1735-1780.
 - [*Recurrent neural networks (RNN)*](https://crl.ucsd.edu/~elman/Papers/fsit.pdf) - Elman, Jeffrey L. “Finding structure in time.” Cognitive science 14.2 (1990): 179-211.
 - [*Generative adversarial networks (GAN)*](https://arxiv.org/pdf/1406.2661v1.pdf) - Goodfellow, Ian, et al. “Generative adversarial nets.” Advances in Neural Information Processing Systems. 2014.
 - [*Deep convolutional inverse graphics networks (DCIGN) *](https://arxiv.org/pdf/1503.03167v4.pdf) - Kulkarni, Tejas D., et al. “Deep convolutional inverse graphics network.” Advances in Neural Information Processing Systems. 2015.
 - [*Deconvolutional networks (DN)*](https://cs.nyu.edu/~fergus/papers/zeilerECCV2014.pdf) - Zeiler, Matthew D., et al. “Deconvolutional networks.” Computer Vision and Pattern Recognition (CVPR), 2010 IEEE Conference on. IEEE, 2010. 
 - [*Convolutional neural networks (CNN or deep convolutional neural networks, DCNN)*](http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf) - LeCun, Yann, et al. “Gradient-based learning applied to document recognition.” Proceedings of the IEEE 86.11 (1998): 2278-2324.
 - [*Deep belief networks (DBN)*](https://papers.nips.cc/paper/3048-greedy-layer-wise-training-of-deep-networks.pdf) -Bengio, Yoshua, et al. “Greedy layer-wise training of deep networks.” Advances in neural information processing systems 19 (2007): 153.
 - [*Denoising autoencoders (DAE)*](http://machinelearning.org/archive/icml2008/papers/592.pdf) - Vincent, Pascal, et al. “Extracting and composing robust features with denoising autoencoders.” Proceedings of the 25th international conference on Machine learning. ACM, 2008.
 - [*Variational autoencoders (VAE) *](https://arxiv.org/pdf/1312.6114v10.pdf) - Kingma, Diederik P., and Max Welling. “Auto-encoding variational bayes.” arXiv preprint arXiv:1312.6114 (2013).
 - [*Sparse autoencoders (SAE) *](https://papers.nips.cc/paper/3112-efficient-learning-of-sparse-representations-with-an-energy-based-model.pdf) - Marc’Aurelio Ranzato, Christopher Poultney, Sumit Chopra, and Yann LeCun. “Efficient learning of sparse representations with an energy-based model.” Proceedings of NIPS. 2007.
 - [*Autoencoders (AE)*](http://ace.cs.ohio.edu/~razvan/courses/dl6890/papers/bourlard-kamp88.pdf) - Bourlard, Hervé, and Yves Kamp. “Auto-association by multilayer perceptrons and singular value decomposition.” Biological cybernetics 59.4-5 (1988): 291-294.
 - [*Restricted Boltzmann machines (RBM)*](http://www.dtic.mil/cgi-bin/GetTRDoc?Location=U2&doc=GetTRDoc.pdf&AD=ADA620727) - Smolensky, Paul. Information processing in dynamical systems: Foundations of harmony theory. No. CU-CS-321-86. COLORADO UNIV AT BOULDER DEPT OF COMPUTER SCIENCE, 1986.
 - [*Boltzmann machines (BM) *](https://www.researchgate.net/profile/Terrence_Sejnowski/publication/242509302_Learning_and_relearning_in_Boltzmann_machines/links/54a4b00f0cf256bf8bb327cc.pdf) - Hinton, Geoffrey E., and Terrence J. Sejnowski. “Learning and releaming in Boltzmann machines.” Parallel distributed processing: Explorations in the microstructure of cognition 1 (1986): 282-317.
 - [*Markov chains (MC or discrete time Markov Chain, DTMC)*](http://www.americanscientist.org/libraries/documents/201321152149545-2013-03Hayes.pdf) - Hayes, Brian. “First links in the Markov chain.” American Scientist 101.2 (2013): 252.
 - [*Hopfield network (HN)*](https://bi.snu.ac.kr/Courses/g-ai09-2/hopfield82.pdf) - Hopfield, John J. “Neural networks and physical systems with emergent collective computational abilities.” Proceedings of the national academy of sciences 79.8 (1982): 2554-2558.
 - [*Radial basis function (RBF) *]() - Broomhead, David S., and David Lowe. Radial basis functions, multi-variable functional interpolation and adaptive networks. No. RSRE-MEMO-4148. ROYAL SIGNALS AND RADAR ESTABLISHMENT MALVERN (UNITED KINGDOM), 1988.
 - [*Feed forward neural networks (FF or FFNN) and perceptrons (P) *](http://www.ling.upenn.edu/courses/cogs501/Rosenblatt1958.pdf) - Rosenblatt, Frank. “The perceptron: a probabilistic model for information storage and organization in the brain.” Psychological review 65.6 (1958): 386.

## Repository of Technical Papers Published about AI, Machine Learning ad Deep Neural Networks - Cornell University

- [*Adaptive Resonance Theory*](https://arxiv.org/search/?query=%22Adaptive+Resonance+Theory%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Artificial Intelligence*](https://arxiv.org/search/?query=AI&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Artificial Neural Network*](https://arxiv.org/search/?query=%22Artificial+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Associate Memory Network*](https://arxiv.org/search/?query=%22Associate+Memory+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Autoencoders*](https://arxiv.org/search/?query=%22Autoencoders%22&searchtype=all&abstracts=show&order=-announced_date_first&size=2050)
- [*Backpropagation Neural Networks*](https://arxiv.org/search/?query=%22Back+Propagation+Neural+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Bag of Words(Classification Method)*]()
- [*Bi-directional Neural Network*](https://arxiv.org/search/?query=%22Bi-directional+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Bigram*](https://arxiv.org/search/?query=%22+Bigram%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Binary Associative Memory*](https://arxiv.org/search/?query=%22Binary+Associative+Memory%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Biological Neural Network*](https://arxiv.org/search/?query=%22Biological+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Cascade correlation*](https://arxiv.org/search/?query=%22Cascade+correlation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Clustering*](https://arxiv.org/search/?query=%22Clustering%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Competitive Learning*](https://arxiv.org/search/?query=%22Competitive+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Compositional pattern-producing network*](https://arxiv.org/search/?query=%22Compositional+pattern-producing+network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Convolutional Neural Networks*](https://arxiv.org/search/?query=%22Convolutional+Neural+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Decoders*](https://arxiv.org/search/?query=%22Decoders%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Deep Feedforward and Recurrent Neural Networks*](https://arxiv.org/search/?query=%22Deep+Feedforward+and+Recurrent+Neural+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Deep Neural Networks*](https://arxiv.org/search/?query=Deep+Neural+Networks&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
-[*Delta Rule*](https://arxiv.org/search/?query=%22Delta+Rule%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Elman Nets*](https://arxiv.org/search/?query=Elman+Nets&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Ensemble Learning*](https://arxiv.org/search/?query=%22Ensemble+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Facial Emotion Recognition Systems*](https://arxiv.org/search/?query=%22Facial+Emotion+Recognition+Systems%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Feedback Network*](https://arxiv.org/search/?query=%22Feedback+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Feedforward - Autoencoders*](https://arxiv.org/search/?query=%22Autoencoders%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Feedforward - Probabilistic*](https://arxiv.org/search/?query=%22Probabilistic%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Feedforward - Time Delay*](https://arxiv.org/search/?query=%22Time+delay%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Feedforward Neural Networks*](https://arxiv.org/search/?query=%22Feedforward+Neural+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Fully recurrent neural network*](https://arxiv.org/search/?query=%22fully+recurrent+neural+network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Generative Adversarial Networks*](https://arxiv.org/search/?query=%22Generative+Adversarial+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Generative Perceptrons*](https://arxiv.org/search/?query=%22Perceptrons%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Genetic Algorithm*](https://arxiv.org/search/?query=%22Genetic+Algorithm%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Gradient Descent Technique*](https://arxiv.org/search/?query=%22Gradient+Descent+Technique%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Gradient Based Methods*](https://arxiv.org/search/?query=%22Gradient+Based+Methods%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Hamming Network*](https://arxiv.org/search/?query=%22Hamming+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Hebbian learning*](https://arxiv.org/search/?query=Hebbian+learning&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Hierarchical Neural Network*](https://arxiv.org/search/?query=%22Hierarchical+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Jordan Networks*](https://arxiv.org/search/?query=%22Jordan+network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*K-means Clustering Algorithm*](https://arxiv.org/search/?query=%22K-means+Clustering+Algorithm%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Kohonen Self Organizing*](https://arxiv.org/search/?query=%22Kohonen+Self+Organizing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
-[*Learning Vector Quantization*](https://arxiv.org/search/?query=%22Learning+Vector+Quantization%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Long Short Term Memory*](https://arxiv.org/search/?query=%22+Long+Short+Term+Memory%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Machine Learning*](https://arxiv.org/search/?query=Machine+Learning&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Modular Neural Network*](https://arxiv.org/search/?query=%22Modular+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Multi-layer feed-forward*](https://arxiv.org/search/?query=%22multi-layer+feed-forward%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Multilayer Perceptrons*](https://arxiv.org/search/?query=%22Multilayer+Perceptrons%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Neighboring topology*](https://arxiv.org/search/?query=%22Neighbor+Topologies%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Neocognitron*](https://arxiv.org/search/?query=%22Neocognitron%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Neuro-fuzzy*](https://arxiv.org/search/?query=%22Neuro-fuzzy%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Optical neural network*](https://arxiv.org/search/?query=%22Optical+neural+network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Papers about Classification*](https://arxiv.org/search/?query=%22Classification%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Radial basis function Neural Network*](https://arxiv.org/search/?query=%22Radial+basis+function+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Radial basis function*](https://arxiv.org/search/?query=%22radial+basis+function%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Recurrent Neural Networks*](https://arxiv.org/search/?query=%22Recurrent+Neural+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*recurrent convolutional neural networks*](https://arxiv.org/search/?query=%22recurrent+convolutional+neural+networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Regulatory feedback*](https://arxiv.org/search/?query=%22Regulatory+feedback%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Reinforcement Learning*](https://arxiv.org/search/?query=%22Reinforcement+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Simulated Annealing*](https://arxiv.org/search/?query=%22Simulated+Annealing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Single-layer feed-forward*](https://arxiv.org/search/?query=%22single-layer+feed-forward%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Spiking Neural Network*](https://arxiv.org/search/?query=%22Spiking+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Stochastic Neural Network*](https://arxiv.org/search/?query=%22Stochastic+Neural+Network%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Supervised Learning*](https://arxiv.org/search/?query=%22Supervised+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Support Vector Machines*](https://arxiv.org/search/?query=%22Support+Vector+Machines%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Temporal Convolutional Networks*](https://arxiv.org/search/?query=%22temporal+Convolutional+Networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Time delay neural network*](https://arxiv.org/search/?query=%22Time+delay+neural+network+%22&searchtype=all&abstracts=show&order=-announced_date_first&size=50)
- [*Training Algorithms*](https://arxiv.org/search/?query=%22Training+Algorithms%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [* Tree Kernels*](https://arxiv.org/search/?query=%22+Tree+Kernels%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Unfolding recurrent neural networks*](https://arxiv.org/search/?query=%22unfolding+recurrent+neural+networks%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Unsupervised learning*](https://arxiv.org/search/?query=%22Unsupervised+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)

## Papers About Areas of Application of Artificial Intelligence and Machine Learning - Cornell University

- [*Automatic Coloration*](https://arxiv.org/search/?query=%22Automatic+Coloration%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Anomaly Detection*](https://arxiv.org/search/?query=%22Sentiment+Analysis%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Automatic Feature Extraction*](https://arxiv.org/search/?query=%22Automatic+Feature+Extraction%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Automatic Machine Translation*](https://arxiv.org/search/?query=%22Automatic+Machine+Translation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Automatic Text Generation*](https://arxiv.org/search/?query=%22Automatic+Text+Generation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Bayesian Learning*](https://arxiv.org/search/?query=%22bayesian+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Character Recognition*](https://arxiv.org/search/?query=%22Character+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Clustering Problems*](https://arxiv.org/search/?query=%22Clustering+Problem%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Computer Vision*](https://arxiv.org/search/?query=%22Computer+Vision%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Contextual Learning*](https://arxiv.org/search/?query=%22Contextual+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Coreference resolution*](https://arxiv.org/search/?query=%22Coreference+resolution%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Denoising*](https://arxiv.org/search/?query=%22Denoising%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Density Estimation and Clustering*](https://arxiv.org/search/?query=%22Density+Estimation+and+Clustering%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Dimensionality reduction*](https://arxiv.org/search/?query=%22+Dimensionality+reduction%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Encrypting Data *](https://arxiv.org/search/?query=%22Encrypting+Data+%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Fraud Detection*](https://arxiv.org/search/?query=%22Fraud+Detection%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Generating Videos with Scene Dynamics*](https://arxiv.org/search/?query=%22Generating+Videos+with+Scene+Dynamics%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Grammar Induction*](https://arxiv.org/search/?query=%22Grammar+Induction%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*High Resolution Image Synthesis*](https://arxiv.org/search/?query=%22High+Resolution+Image+Synthesis%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Human Face Recognition*](https://arxiv.org/search/?query=%22Face+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Classification and Localization*](https://arxiv.org/search/?query=%22Image+Classification+and+Localization%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Captioning*](https://arxiv.org/search/?query=%22Image+Captioning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Generation*](https://arxiv.org/search/?query=%22Image+Generation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Manipulation*](https://arxiv.org/search/?query=%22Image+Manipulation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Processing*](https://arxiv.org/search/?query=%22+Image+Processing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Recognition*](https://arxiv.org/search/?query=%22Image+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Restoration*](https://arxiv.org/search/?query=%22Image+Restoration%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image Segmentation*](https://arxiv.org/search/?query=%22Image+Segmentation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Image to Image Translation*](https://arxiv.org/search/?query=%22Image+to+Image+Translation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Increase Image Resolution*](https://arxiv.org/search/?query=%22Increase+Image+Resolution%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Inductive Learning*](https://arxiv.org/search/?query=%22Inductive+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Integrated Recognition*](https://arxiv.org/search/?query=%22Integrated+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Intelligent Agents*](https://arxiv.org/search/?query=%22Intelligent+Agents%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Intelligent Assistants*](https://arxiv.org/search/?query=%22Intelligent+Assistants%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Iris Classification Problem*](https://arxiv.org/search/?query=%22Iris+Classification+Problem%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Labeling Images*](https://arxiv.org/search/?query=%22Labeling+Images%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Language Generation *](https://arxiv.org/search/?query=%22Language+Generation+%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Language Understanding*](https://arxiv.org/search/?query=%22Language+Understanding%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Machine Reading*](https://arxiv.org/search/?query=%22Machine+Reading%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Machine Translation*](https://arxiv.org/search/?query=%22Machine+Translation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Mobile Vision*](https://arxiv.org/search/?query=%22Mobile+Vision%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Multi-document Summarization*](https://arxiv.org/search/?query=%22Multi-document+Summarization%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Named Entity Recognition*](https://arxiv.org/search/?query=%22Named+Entity+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Natural Language Processing*](https://arxiv.org/search/?query=%22Natural+Language+Processing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Object Detection*](https://arxiv.org/search/?query=%22Object+Detection%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Object Detection from Video*](https://arxiv.org/search/?query=%22Object+Detection+from+Video%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Paraphrase Detection*](https://arxiv.org/search/?query=%22Paraphrase+Detection%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Pattern Recognition*](https://arxiv.org/search/?query=%22Pattern+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Question Answering*](https://arxiv.org/search/?query=%22Question+Answering%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Real time object recognition*](https://arxiv.org/search/?query=%22Real+time+object+recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Recommendation Systems*](https://arxiv.org/search/?query=%22Recommendation+Systems%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Scene Classification*](https://arxiv.org/search/?query=%22Scene+Classification%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Scene Parsing*](https://arxiv.org/search/?query=%22Scene+Parsing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Sequence Labeling*](https://arxiv.org/search/?query=%22Sequence+Labeling%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Self Driving Cars*](https://arxiv.org/search/?query=%22Self+Driving+Cars%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Semantic Folding*](https://arxiv.org/search/?query=%22Semantic+Folding%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Semantic Parsing*](https://arxiv.org/search/?query=%22Semantic+Parsing+%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Semantic Segmentation*](https://arxiv.org/search/?query=%22Semantic+Segmentation%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Sentiment Analysis*](https://arxiv.org/search/?query=%22Sentiment+Analysis%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Sentiment Analysis for Short Texts*](https://arxiv.org/search/?query=%22Sentiment+Analysis+for+Short+Texts%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Sequence to Sequence Learning*](https://arxiv.org/search/?query=%22Sequence+to+Sequence+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Signature Verification Application*](https://arxiv.org/search/?query=%22Signature+Verification%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Speech Processing*](https://arxiv.org/search/?query=%22Speech+Processing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Speech Recognition*](https://arxiv.org/search/?query=%22Speech+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Speech Tagging*](https://arxiv.org/search/?query=%22Speech+Tagging%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Spell Checking*](https://arxiv.org/search/?query=%22Spell+Checking%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Structured Prediction*](https://arxiv.org/search/?query=%22Structured+Prediction%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Super Resolution*](https://arxiv.org/search/?query=%22Super+Resolution%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Text Categorization*](https://arxiv.org/search/?query=%22Text+Categorization%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Text Classification*](https://arxiv.org/search/?query=%22Text+Classification%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Textual Entailment*](https://arxiv.org/search/?query=%22Textual+Entailment%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Text Recognition*](https://arxiv.org/search/?query=%22Text+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Thought Vector*](https://arxiv.org/search/?query=%22Thought+Vector%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Transfer Learning*](https://arxiv.org/search/?query=%22Transfer+Learning%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Urban Computing*](https://arxiv.org/search/?query=%22Urban+Computing%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Video-to-Video Synthesis*](https://arxiv.org/search/?query=%22Video-to-Video+Synthesis%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Visual Recognition*](https://arxiv.org/search/?query=%22Visual+Recognition%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)
- [*Voice Search*](https://arxiv.org/search/?query=%22Voice+Search%22&searchtype=all&abstracts=show&order=-announced_date_first&size=200)

## Machine Learning Glossary
This glossary defines general machine learning terms as well as terms specific to TensorFlow.
   
- [*Machine Learning Glossary*](https://developers.google.com/machine-learning/glossary/)